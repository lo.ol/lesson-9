#include <iostream>
#include <string>
#include "BSNode.cpp"
#define SIZE 15
using namespace std;
int main(void)
{
	int intArr[SIZE] = { 1,8,5,9,6,2,3,5,7,48,66,4,14,25,73 };
	string stringArr[SIZE] = {"pizza","drink","chair","rain","maybe","fall","ice","love","name","dollar","sea","help"
		,"mountain","cherry","sun" };
	BSNode<int>* ints = new BSNode<int>(intArr[0]);
	BSNode<string>* strings = new BSNode<string>(stringArr[0]);
	cout << "int array values:" << endl;
	cout << intArr[0] << " ";
	for (int i = 1; i < SIZE; i++)
	{
		cout << intArr[i] << " ";
		ints->insert(intArr[i]);
	}
	cout << "\nstring array values:" << endl;
	cout << stringArr[0] << " ";
	for (int i = 1; i < SIZE; i++)
	{
		cout << stringArr[i] << " ";
		strings->insert(stringArr[i]);
	}
	cout << "int array after sort: ";
	ints->printNodes();
	cout << "string array after sort: ";
	strings->printNodes();
	delete ints;
	delete strings;
	system("Pause");
	return 0;
}