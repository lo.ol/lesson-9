#include "BSNode.h"

BSNode::BSNode(string data)
{
	if (data.find('#') == string::npos && data.find(' ') == string::npos)
	{
		this->_data = data;
	}
	
	this->_left = nullptr;
	this->_right = nullptr;
	this->_count = 1;
}
/*constractor with node*/
BSNode::BSNode(const BSNode & other)
{
	this->_data = other.getData();
	this->_count = other.getCount();
	if (other.getLeft() != nullptr)
	{
		this->_left = new BSNode(*other.getLeft());
	}
	if (other.getRight() != nullptr)
	{
		this->_right = new BSNode(*other.getRight());
	}
	
	
	
}

BSNode::~BSNode()
{
	deleteTree();
	
}

/*puts a value in tree*/
void BSNode::insert(string value)
{
	//if value is already in tree , 1 is added to count
	if (this->search(value) == true && this->_data == value)
	{
		
		this->_count += 1;
	}
	else
	{
		//check if value goes to right or left node
		if (value < this->getData())
		{
			//addes value to end of left node
			if (this->getLeft() == nullptr)
			{
				this->_left = new BSNode(value);
			}
			else
			{
				(this->getLeft())->insert(value);
			}

		}
		else {
			//addes value to end of right node
			if (this->getRight() == nullptr)
			{
				this->_right = new BSNode(value);
			}
			else
			{
				(this->getRight())->insert(value);
			}
		}
	}
	
	
	
}

BSNode & BSNode::operator=(const BSNode & other)
{
	BSNode newTree(other);

	return newTree;
}


/*checks if node doesnt have left and right*/
bool BSNode::isLeaf() const
{
	if (this->getLeft() == nullptr && this->getRight() == nullptr)
	{
		return true;
	}
	return false;
}

string BSNode::getData() const
{
	return _data;
}

BSNode * BSNode::getLeft() const
{
	return _left;
}

BSNode * BSNode::getRight() const
{
	return _right;
}

int BSNode::getCount() const
{
	return _count;
}
/*search for a value in a given tree*/
bool BSNode::search(string val) const
{
	bool found = false;
	if (this->_data == val)
	{
		return true;
	}
	if (this->getRight() != nullptr)
	{
		found = (this->getRight())->search(val);
	}
	if (this->getLeft() != nullptr)
	{
		found = (this->getLeft())->search(val);
	}
	return found;
}

/*calculate height of tree*/
int BSNode::getHeight() const
{
	int heightl = 1, heightr = 1;
	
	if (this->getLeft() != nullptr)
	{
		heightl += (this->getLeft())->getHeight();
	}
	if (this->getRight() != nullptr)
	{
		heightr += (this->getRight())->getHeight();
	}
	return max(heightl,heightr);
}

/*culculates depth */
int BSNode::getDepth(const BSNode & root) const
{
	int depth = 0;
	if (root.search(this->_data) == false)
	{
		return -1;
	}
	else
	{
		depth = this->getCurrNodeDistFromInputNode(&root);
	}
	return depth;
}

/*returns the bigger number*/
int BSNode::max(int x, int y) const
{
	if (x < y)
	{
		return y;
	}
	return x;
}

void BSNode::printNodes() const
{
	
	/* first recur on left child */
	if (this->getLeft() != nullptr)
	{
		this->getLeft()->printNodes();
	}
	

	/* then print the data of node */
	cout << this->_data << " " << this->_count << endl;

	/* now recur on right child */
	if (this->getRight() != nullptr)
	{
		this->getRight()->printNodes();
	}
	
}

void BSNode::deleteTree()
{
	if (_left != nullptr)
	{
		_left->deleteTree();
		delete _left;

	}
		
	
	if (_right != nullptr)
	{
		_right->deleteTree();
		delete _right;
	}
	
}


/*gets the distanse between two nodes*/
int BSNode::getCurrNodeDistFromInputNode(const BSNode * node) const
{
	
	return node->getHeight() - this->getHeight();
}


