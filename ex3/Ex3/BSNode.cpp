#include "BSNode.h"

template <class T>
BSNode<T>::BSNode(T data)
{
	
	this->_data = data;
	
	this->_left = nullptr;
	this->_right = nullptr;
	this->_count = 1;
}
template <class T>
/*constractor with node*/
BSNode<T>::BSNode(const BSNode & other)
{
	this->_data = other.getData();
	this->_count = other.getCount();
	if (other.getLeft() != nullptr)
	{
		this->_left = new BSNode(*other.getLeft());
	}
	if (other.getRight() != nullptr)
	{
		this->_right = new BSNode(*other.getRight());
	}



}
template <class T>
BSNode<T>::~BSNode()
{
	deleteTree();
	
}
template <class T>
/*puts a value in tree*/
void BSNode<T>::insert(T value)
{
	//if value is already in tree , 1 is added to count
	if (this->search(value) == true && this->_data == value)
	{

		this->_count += 1;
	}
	else
	{
		//check if value goes to right or left node
		if (value < this->getData())
		{
			//addes value to end of left node
			if (this->getLeft() == nullptr)
			{
				this->_left = new BSNode(value);
			}
			else
			{
				(this->getLeft())->insert(value);
			}

		}
		else {
			//addes value to end of right node
			if (this->getRight() == nullptr)
			{
				this->_right = new BSNode(value);
			}
			else
			{
				(this->getRight())->insert(value);
			}
		}
	}



}
template <class T>
BSNode<T> & BSNode<T>::operator=(const BSNode & other)
{
	BSNode newTree(other);

	return newTree;
}

template <class T>
/*checks if node doesnt have left and right*/
bool BSNode<T>::isLeaf() const
{
	if (this->getLeft() == nullptr && this->getRight() == nullptr)
	{
		return true;
	}
	return false;
}
template <class T>
T BSNode<T>::getData() const
{
	return _data;
}
template <class T>
BSNode<T> * BSNode<T>::getLeft() const
{
	return _left;
}
template <class T>
BSNode<T> * BSNode<T>::getRight() const
{
	return _right;
}
template <class T>
int BSNode<T>::getCount() const
{
	return _count;
}
template <class T>
/*search for a value in a given tree*/
bool BSNode<T>::search(T val) const
{
	bool found = false;
	if (this->_data == val)
	{
		return true;
	}
	if (this->getRight() != nullptr)
	{
		found = (this->getRight())->search(val);
	}
	if (this->getLeft() != nullptr)
	{
		found = (this->getLeft())->search(val);
	}
	return found;
}
template <class T>
/*calculate height of tree*/
int BSNode<T>::getHeight() const
{
	int heightl = 1, heightr = 1;

	if (this->getLeft() != nullptr)
	{
		heightl += (this->getLeft())->getHeight();
	}
	if (this->getRight() != nullptr)
	{
		heightr += (this->getRight())->getHeight();
	}
	return max(heightl, heightr);
}
template <class T>
/*culculates depth */
int BSNode<T>::getDepth(const BSNode & root) const
{
	int depth = 0;
	if (root.search(this->_data) == false)
	{
		return -1;
	}
	else
	{
		depth = this->getCurrNodeDistFromInputNode(&root);
	}
	return depth;
}
template <class T>
/*returns the bigger number*/
int BSNode<T>::max(int x, int y) const
{
	if (x < y)
	{
		return y;
	}
	return x;
}
template <class T>
void BSNode<T>::printNodes() const
{

	/* first recur on left child */
	if (this->getLeft() != nullptr)
	{
		this->getLeft()->printNodes();
	}


	/* then print the data of node */
	cout << this->_data << " " << this->_count << endl;

	/* now recur on right child */
	if (this->getRight() != nullptr)
	{
		this->getRight()->printNodes();
	}

}



template <class T>
/*gets the distanse between two nodes*/
int BSNode<T>::getCurrNodeDistFromInputNode(const BSNode * node) const
{

	return node->getHeight() - this->getHeight();
}

template <class T>
void BSNode<T>::deleteTree()
{
	if (_left != nullptr)
	{
		_left->deleteTree();
		delete _left;

	}


	if (_right != nullptr)
	{
		_right->deleteTree();
		delete _right;
	}

}