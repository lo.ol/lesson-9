#ifndef BSNode_H
#define BSNode_H

#include <iostream>
#include <string>

using namespace std;
template <class T>
class BSNode
{
	
public:
	
	BSNode(T data);
	
	BSNode(const BSNode& other);

	~BSNode();
	
	void insert(T value);
	BSNode& operator=(const BSNode& other);

	bool isLeaf() const;
	
	T getData() const;
	BSNode* getLeft() const;
	BSNode* getRight() const;
	int getCount() const;
	bool search(T val) const;

	int getHeight() const;
	int getDepth(const BSNode& root) const;
	int max(int x, int y) const;
	void printNodes() const; //for question 1 part C*/
	void BSNode<T>::deleteTree()
	;

private:
	T _data;
	BSNode<T>* _left;
	BSNode<T>* _right;

	int _count; //for question 1 part B
	int getCurrNodeDistFromInputNode(const BSNode<T>* node) const; //auxiliary function for getDepth

};

#endif