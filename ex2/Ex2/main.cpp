#include "functions.h"
#include "len.h"


using namespace std;

int main() {

	
//check compare
	cout << "correct print is 1 -1 0" << endl;
	std::cout << compare<double>(1.0, 2.5) << std::endl;
	std::cout << compare<double>(4.5, 2.4) << std::endl;
	std::cout << compare<double>(4.4, 4.4) << std::endl;

//check bubbleSort
	std::cout << "correct print is sorted array" << std::endl;

	const int arr_size = 5;
	double doubleArr[arr_size] = { 1000.0, 2.0, 3.4, 17.0, 50.0 };
	bubbleSort<double>(doubleArr, arr_size);
	for ( int i = 0; i < arr_size; i++ ) {
		std::cout << doubleArr[i] << " ";
	}
	std::cout << std::endl;

//check printArray
	std::cout << "correct print is sorted array" << std::endl;
	printArray<double>(doubleArr, arr_size);
	std::cout << std::endl;


	//check compare
	std::cout << "correct print is 1 1" << std::endl;
	std::cout << compare<char>('a', 'c') << std::endl;
	std::cout << compare<char>('d', 'x') << std::endl;
	

	//check bubbleSort
	

	
	char charArr[arr_size] = { 'a', 'd', 'r', 't', 'p' };
	std::cout << "print not  sorted array" << std::endl;
	printArray<char>(charArr, arr_size);
	bubbleSort<char>(charArr, arr_size);

	//check printArray
	std::cout << "print sorted array" << std::endl;
	printArray<char>(charArr, arr_size);
	std::cout << std::endl;

	len l(6);
	len a(0);
	
	//check compare
	std::cout << "correct print is -1 0" << std::endl;
	std::cout << compare<len>(l, a) << std::endl;
	a = l;
	std::cout << compare<len>(l, a) << std::endl;


	//check bubbleSort
	
	len z(8);
	len x(56);
	len y(9);
	
	len lenArr[arr_size] = { l, a, x, z, y };
	std::cout << "print not sorted array" << std::endl;
	printArray<len>(lenArr, arr_size);
	bubbleSort<len>(lenArr, arr_size);

	//check printArray
	std::cout << "print is sorted array" << std::endl;
	printArray<len>(lenArr, arr_size);
	std::cout << std::endl;
	
	system("pause");
	return 1;
}