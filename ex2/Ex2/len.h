#pragma once
#include <stdio.h>
#include <iostream>
class len
{
public:
	len(int x);
	~len();
	int _len;

	bool operator<(const len& x);
	bool operator==(const len& x);
	void operator=(const len& x);
	friend std::ostream & operator<<(std::ostream& s, const len& val);
	
};

