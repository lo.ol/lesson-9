#include "len.h"



len::len(int x) : _len(x)
{
}


len::~len()
{
}

bool len::operator<(const len& x)
{
	return _len < x._len;
}

bool len::operator==(const len& x)
{
	return _len == x._len;
}

void len::operator=(const len& x)
{
	this->_len = x._len;
	
}


std::ostream & operator<<(std::ostream & s, const len & val)
{
	s << val._len;
	return s;
}
