#include "BSNode.h"
#pragma comment(lib, "..\\Debug\\printTreeToFile.lib")
#include <iostream>
#include <fstream>
#include <string>
#include <windows.h>
#include "..\\Debug\printTreeToFile.h"
using namespace std;


int main()
{
	BSNode* bs = new BSNode("6");
	BSNode* tree = new BSNode("1");
	bs->insert("2");
	bs->insert("8");
	bs->insert("3");
	bs->insert("5");
	bs->insert("9");
	bs->insert("6");
	
	tree->insert("78");
	tree->insert("1");
	tree->insert("78");
	tree->insert("55");
	tree->insert("89");
	tree->insert("55");
	tree->insert("41");
	tree->insert("89");
	tree->insert("41");

	cout<< bs->search("12");
	
	cout << "Tree height: " << bs->getHeight() << endl;
	cout << "depth of node with 5 depth: " << bs->getLeft()->getRight()->getRight()->getDepth(*bs) << endl;
	cout << "depth of node with 3 depth: " << bs->getLeft()->getRight()->getDepth(*bs) << endl;
	bs->printNodes();
	cout << "tree nodes:" << endl;
	tree->printNodes();
	string textTree = "BSTData.txt";
	printTreeToFile(tree, textTree);

	system("BinaryTree.exe");
	system("pause");
	remove(textTree.c_str());
	delete bs;
	delete tree;

	return 0;
}

