#pragma once

#include <iostream>
#define EQUAL 0
#define BIGGER -1
#define SMALLER 1


	


	template <typename T>
	int compare(T param1, T param2)
	{
		if (param1 < param2)
		{
			return SMALLER;
		}
		else if (param2 == param1)
		{
			return EQUAL;
		}
		else
		{
			return BIGGER;
		}
	}


	template <typename T>
	void swap(T& first, T& second)
	{
		T temp = first;
		first = second;
		second = temp;
	}

	template <typename T>
	void bubbleSort(T* arr, int size)
	{
		int i, j;
		for (i = 0; i < size - 1; i++)
		{
			// Last i elements are already in place    
			for (j = 0; j < size - i - 1; j++)
			{
				if (arr[j + 1] < arr[j])
				{
					swap(arr[j], arr[j + 1]);
				}
			}
		}
	}



	template <typename T>
	void printArray(const T* arr, int size)
	{

		for (int i = 0; i < size; i++)
		{
			std::cout << arr[i] << std::endl;
		}

	}










